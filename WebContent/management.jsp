<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link href="./css/style.css" rel="stylesheet" type="text/css">
<title>management</title>
<script type="text/javascript">
	function activateConfirm() {
		if (confirm("本当に復活させますか？")) {
			alert("復活しました");
			return true;
		} else {
			return false;
		}
	}

	function deactivateConfirm() {
		if (confirm("本当に停止させますか？")) {
			alert("停止しました");
			return true;
		} else {
			return false;
		}
	}
</script>
</head>
<body>
	<div class="link">
		<a href="./">ホーム</a>
		<a href="signup">ユーザー新規登録</a>
	</div>
	<div class="error-messages">
		<c:if test="${ not empty errorMessages }">
			<c:forEach items="${ errorMessages }" var="errorMessage">
				<ul>
					<li><c:out value="${errorMessage}"/></li>
				</ul>
			</c:forEach>
			<c:remove var="errorMessages" scope="session"/>
		</c:if>
	</div>
	<br>
	<div class="users-info-wrapper">
		<div class="users-info">
			<c:forEach items="${ userBranchDepartments }" var="user">
				<table>
					<tr><td colspan="2"><c:out value="${ user.account }"/></td></tr>
					<tr><td colspan="2"><c:out value="${ user.name }"/></td></tr>
					<tr><td colspan="2"><c:out value="${ user.branchName }"/></td></tr>
					<tr><td colspan="2"><c:out value="${ user.departmentName }"/></td></tr>
					<tr>
						<c:choose>
							<c:when test="${ user.isStopped == 0 }">
								<td>有効</td>
							</c:when>
							<c:otherwise>
								<td>停止中</td>
							</c:otherwise>
						</c:choose>
					</tr>
					<tr>
						<td>
							<form action="setting" method="get">
								<input type="hidden" name="userId" value="${ user.id }"/>
								<input type="submit" value="編集"/>
							</form>
						</td>
						<td>
							<c:if test="${ user.id != loginUser.id }">
								<c:choose>
									<c:when test="${ user.isStopped == 0 }">
										<form action="stop" method="post"onclick="return deactivateConfirm()">
											<input type="hidden" name="userId" value="${ user.id }"/>
											<input type="hidden" name="isStopped" value="1"/>
											<input type="submit" value="停止"/>
										</form>
									</c:when>
									<c:otherwise>
										<form action="stop" method="post" onclick="return activateConfirm()">
											<input type="hidden" name="userId" value="${ user.id }"/>
											<input type="hidden" name="isStopped" value="0"/>
											<input type="submit" value="復活"/>
										</form>
									</c:otherwise>
								</c:choose>
							</c:if>
						</td>
					</tr>
				</table>
				<br>
			</c:forEach>
		</div>
	</div>
	<div class="copyright">Copyright(c)takanari</div>
</body>
</html>