<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link href="./css/style.css" rel="stylesheet" type="text/css">
<title>ホーム画面</title>
<script type="text/javascript">
	function deleteConfirm() {
		if (confirm("本当に削除しますか？")) {
			alert("削除しました");
			return true;
		} else {
			return false;
		}
	}
</script>
</head>
<body>
	<div class="link">
		<a href="message">新規投稿</a>
		<a href="management">ユーザー管理</a>
		<a href="logout">ログアウト</a>
	</div>
	<div class="error-messages">
		<c:if test="${ not empty errorMessages }">
			<c:forEach items="${ errorMessages }" var="errorMessage">
				<ul>
					<li><c:out value="${errorMessage}"/></li>
				</ul>
			</c:forEach>
			<c:remove var="errorMessages" scope="session"/>
		</c:if>
	</div>
	<div class="filter">
		<form action="./" method="get" class="filter-form">
			<table class="filter-table">
				<tr>
					<td><label for="date">日付</label></td>
					<td><input type="date" name="start" value="${ selectedStart }" id="date" class="input-field"/></td>
					<td><p>～</p></td>
					<td><input type="date" name="end" value="${ selectedEnd }" class="input-field"/></td>
				</tr>
				<tr>
					<td><label for="category">カテゴリ</label></td>
					<td colspan="2"><input type="text" name="category" value="${ selectedCategory }" id="category" class="input-field"/></td>
					<td><input type="submit" value="絞込み"/>
				</tr>
			</table>
		</form>
	</div>
	<br>
	<c:if test="${ not empty userMessages }">
		<div class="message-comment-list">
			<c:forEach items="${userMessages}" var="userMessage">
				<div class="message-comment-set">
					<div class="message-table-wrapper">
						<table class="message-table">
							<tr><td><c:out value="${ userMessage.userName }" /></td></tr>
							<tr><td><c:out value="${ userMessage.title }" /></td></tr>
							<tr><td><c:out value="${ userMessage.category }" /></td></tr>
							<tr>
								<td>
									<c:set var="newLine" value="\\r\\n"/>
									<c:forEach items='${ userMessage.text.split(newLine) }' var="text">
										<c:out value="${ text }"/>
										<br>
									</c:forEach>
								</td>
							</tr>
							<tr><td><fmt:formatDate value="${ userMessage.createdDate }" pattern="yyyy/MM/dd HH:mm:ss"/></td></tr>
						</table>
					</div>
					<div class="message-delete-button">
						<c:if test="${ userMessage.userId == loginUser.id }">
							<form action="deleteMessage" method="post" onclick="return deleteConfirm()">
								<input type="hidden" name="id" value="${ userMessage.id }" >
								<input type="submit" value="削除">
							</form>
						</c:if>
					</div>
					<br>
					<div class="comment-list">
						<c:forEach items="${ userComments }" var="userComment">
							<c:if test="${ userMessage.id == userComment.messageId }">
								<div class="comment-table">
									<table>
										<tr><td><c:out value="${ userComment.userName }"/></td></tr>
										<tr>
											<td>
												<c:forEach items="${ userComment.text.split(newLine) }" var="comment">
													<c:out value="${ comment }"/>
													<br>
												</c:forEach>
											</td>
											<!--
												<td>
													<c:out value='${fn:replace(userMessage.text, newLine, "<br>")}'/>
												</td>
											-->
										</tr>
										<tr><td><fmt:formatDate value="${ userComment.createdDate }" pattern="yyyy/MM/dd HH:mm:ss"/></td></tr>
									</table>
								</div>
								<div class="comment-delete-button">
									<c:if test="${ userComment.userId == loginUser.id }">
										<form action="deleteComment" method="post" onclick="return deleteConfirm()">
											<input type="hidden" name="id" value="${ userComment.id }"/>
											<input type="submit" value="削除"/>
										</form>
									</c:if>
								</div>
								<br>
							</c:if>
						</c:forEach>
					</div>
					<div class="comment-form">
						<form action="comment" method="post">
							<input type="hidden" name="messageId" value="${ userMessage.id }"/>
							<label for="text">コメント入力欄</label><br>
							<textarea name="text" id="text" class="comment-area" cols="" rows="4"></textarea>
							<br>
							<input type="submit" class="comment-post-button" value="コメント投稿"/>
						</form>
					</div>
				</div>
			</c:forEach>
		</div>
	</c:if>
	<div class="copyright"><p>Copyright(c)takanari</p></div>
</body>
</html>