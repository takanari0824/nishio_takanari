<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link href="./css/style.css" rel="stylesheet" type="text/css">
<title>login</title>
</head>
<body>
	<div class="error-messages">
		<c:if test="${ not empty errorMessages }">
			<c:forEach items="${ errorMessages }" var="errorMessage">
				<ul>
					<li><c:out value="${errorMessage}"/></li>
				</ul>
			</c:forEach>
			<c:remove var="errorMessages" scope="session"/>
		</c:if>
	</div>
	<div class="login-form">
		<form action="login" method="post">
			<table class="login-form-table">
				<tr>
					<td><label for="account">アカウント</label></td>
					<td><input type="text" name="account" id="account" value="${ account }"/></td>
				</tr>
				<tr>
					<td><label for="password">パスワード</label></td>
					<td><input type="password" name="password" id="password"/></td>
				</tr>
				<tr>
					<td><input type="submit" value="ログイン"/></td>
				</tr>
			</table>
			<div class="copyright"> Copyright(c)takanari</div>
		</form>
	</div>
</body>
</html>