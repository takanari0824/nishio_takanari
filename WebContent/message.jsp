<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link href="./css/style.css" rel="stylesheet" type="text/css">
<title>新規投稿画面</title>
</head>
<body>
	<div class="link">
		<a href="./">ホーム</a>
	</div>
	<div class="error-messages">
		<c:if test="${ not empty errorMessages }">
			<c:forEach items="${ errorMessages }" var="errorMessage">
				<ul>
					<li><c:out value="${errorMessage}"/></li>
				</ul>
			</c:forEach>
		</c:if>
	</div>
	<div class="message-form">
		<form action="message" method="post">
			<table class="message-form-table">
				<tr>
					<td><label for="title">件名</label></td>
					<td><input type="text" name="title" id="title" class="input-field" value="${ message.title }"/></td>
				</tr>
				<tr>
					<td><label for="category">カテゴリ</label></td>
					<td><input type="text" name="category" id="category"class="input-field"  value="${ message.category }"/></td>
				</tr>
				<tr>
					<td><label for="text">投稿内容</label></td>
					<td><textarea name="text" id="text" cols=""class="input-field" rows="4">${ message.text }</textarea></td>
				</tr>
				<tr>
					<td><input type="submit" value="投稿"/></td>
					<td></td>
				</tr>
			</table>
		</form>
	</div>
	<div class="copyright"><p>Copyright(c)takanari</p></div>
</body>
</html>