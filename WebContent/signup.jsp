<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link href="./css/style.css" rel="stylesheet" type="text/css">
<title>signup</title>
</head>
<body>
	<div class="link">
		<a href="management">ユーザー管理</a>
	</div>
	<div class="error-messages">
		<c:if test="${ not empty errorMessages }">
			<c:forEach items="${ errorMessages }" var="errorMessage">
				<ul>
					<li><c:out value="${errorMessage}"/></li>
				</ul>
			</c:forEach>
			<c:remove var="errorMessages" scope="session"/>
		</c:if>
	</div>
	<br>
	<div class="signup-form-wrapper">
		<form action="signup" method="post">
			<table class="signup-form-table">
				<tr>
					<td><label for="account">アカウント</label></td>
					<td><input type="text" value="${ user.account }" name="account" id="account"/></td>
				</tr>
				<tr>
					<td><label for="password">パスワード</label></td>
					<td><input type="password" name="password" id="password"/></td>
				</tr>
				<tr>
					<td><label for="passwordConfirmation">確認用パスワード</label></td>
					<td><input type="password" name="passwordConfirmation" id="passwordConfirmation"/></td>
				</tr>
				<tr>
					<td><label for="name">名前</label></td>
					<td><input type="text" value="${ user.name }" name="name" id="name"/></td>
				</tr>
				<tr>
					<td><label for="branchId">支社</label></td>
					<td>
						<select name="branchId" id="branchId">
							<c:forEach items="${ branchList }" var="branch">
								<c:choose>
									<c:when test="${ user.branchId == branch.id }">
										<option value="${ branch.id }" selected><c:out value="${ branch.name }"/></option>
									</c:when>
									<c:otherwise>
										<option value="${ branch.id }"><c:out value="${ branch.name }"/></option>
									</c:otherwise>
								</c:choose>
							</c:forEach>
						</select>
					</td>
				</tr>
				<tr>
					<td><label for="departmentId">部署</label></td>
					<td>
						<select name="departmentId" id="departmentId">
							<c:forEach items="${ departmentList }" var="department">
								<c:choose>
									<c:when test="${ user.departmentId == department.id }">
										<option value="${ department.id }" selected><c:out value="${ department.name }"/></option>
									</c:when>
									<c:otherwise>
										<option value="${ department.id }"><c:out value="${ department.name }"/></option>
									</c:otherwise>
								</c:choose>
							</c:forEach>
						</select>
					</td>
				</tr>
				<tr>
					<td><input type="submit" value="登録"/></td>
					<td></td>
				</tr>
			</table>
		</form>
	</div>

	<div class="copyright"><p>Copyright(c)takanari</p></div>
</body>
</html>