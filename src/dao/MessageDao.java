package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import beans.Message;
import exception.SQLRuntimeException;

public class MessageDao {

	public void insert(Connection connection, Message message) {
		PreparedStatement ps = null;

		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO messages (");
			sql.append("title,");
			sql.append("text,");
			sql.append("category,");
			sql.append("user_id");
			sql.append(") VALUES (");
			sql.append("?,");
			sql.append("?,");
			sql.append("?,");
			sql.append("?");
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, message.getTitle());
			ps.setString(2, message.getText());
			ps.setString(3, message.getCategory());
			ps.setInt(4, message.getUserId());

			ps.executeUpdate();
		}catch(SQLException e) {
			throw new SQLRuntimeException(e);
		}finally {
			close(ps);
		}
	}

	public void delete(Connection connection, int id) {
		PreparedStatement ps = null;

		try {
			String sql = "DELETE FROM messages where id = ?";
			ps = connection.prepareStatement(sql);
			ps.setInt(1, id);

			int updateCount = ps.executeUpdate();
			if(updateCount == 1) {
				System.out.println("削除成功");
			} else {
				System.out.println("削除失敗");
			}
		} catch(SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

}
