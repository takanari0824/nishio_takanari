package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.UserComment;
import exception.SQLRuntimeException;

public class UserCommentDao {


	public List<UserComment> select(Connection connection){
		PreparedStatement ps = null;

		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("comments.id AS id, ");
			sql.append("comments.text AS text, ");
			sql.append("comments.user_id AS userId, ");
			sql.append("users.name AS userName, ");
			sql.append("comments.message_id AS messageId, ");
			sql.append("comments.created_date AS createdDate, ");
			sql.append("comments.updated_date AS updatedDate ");
			sql.append("FROM comments ");
			sql.append("INNER JOIN users ");
			sql.append("ON comments.user_id = users.id ");
			sql.append("ORDER BY createdDate ASC");

			ps = connection.prepareStatement(sql.toString());
			ResultSet rs = ps.executeQuery();

			List<UserComment> userComments = toUserComments(rs);
			return userComments;
		} catch(SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<UserComment> toUserComments(ResultSet rs) throws SQLException{
		List<UserComment> userComments = new ArrayList<>();

		try {
			while(rs.next()) {
				UserComment userComment = new UserComment();
				userComment.setId(rs.getInt("id"));
				userComment.setText(rs.getString("text"));
				userComment.setUserId(rs.getInt("userId"));
				userComment.setUserName(rs.getString("userName"));
				userComment.setMessageId(rs.getInt("messageId"));
				userComment.setCreatedDate(rs.getTimestamp("createdDate"));
				userComment.setUpdatedDate(rs.getTimestamp("updatedDate"));
				userComments.add(userComment);
			}
			return userComments;
		} finally {
			close(rs);
		}
	}
}
