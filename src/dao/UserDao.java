package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import beans.User;
import exception.NoRowsUpdatedRuntimeException;
import exception.SQLRuntimeException;

public class UserDao {

	public void insert(Connection connection, User user) {
		PreparedStatement ps = null;

		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO users (");
			sql.append( "account,");
			sql.append("password,");
			sql.append("name,");
			sql.append("branch_id,");
			sql.append("department_id");
			sql.append(") VALUES (");
			sql.append("?,");
			sql.append("?,");
			sql.append("?,");
			sql.append("?,");
			sql.append("?");
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, user.getAccount());
			ps.setString(2, user.getPassword());
			ps.setString(3, user.getName());
			ps.setInt(4, user.getBranchId());
			ps.setInt(5, user.getDepartmentId());

			ps.executeUpdate();
		} catch(SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public User select(Connection connection,String account, String encPassword) {
		PreparedStatement ps = null;

		try {
			String sql = "SELECT * FROM users WHERE account = ? AND password = ?";
			ps = connection.prepareStatement(sql);

			ps.setString(1, account);
			ps.setString(2, encPassword);

			ResultSet rs = ps.executeQuery();

			List<User> users = new ArrayList<>();
			users = toUsers(rs);

			if(users.size() == 0) {
				return null;
			} else if(users.size() >= 2) {
				throw new IllegalStateException("ユーザーが重複しています");
			} else {
				return users.get(0);
			}
		} catch(SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public User select(Connection connection, String account) {
		PreparedStatement ps = null;

		try {
			String sql = "SELECT * FROM users WHERE account = ?";
			ps = connection.prepareStatement(sql);
			ps.setString(1,account);

			ResultSet rs = ps.executeQuery();
			List<User> users = new ArrayList<>();
			users = toUsers(rs);

			if(users.size() == 0) {
				return null;
			} else {
				return users.get(0);
			}
		} catch(SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public User select(Connection connection, int userId) {
		PreparedStatement ps = null;

		try {
			String sql = "SELECT * FROM users WHERE id = ?";
			ps = connection.prepareStatement(sql);
			ps.setInt(1, userId);
			ResultSet rs = ps.executeQuery();
			List<User> users = toUsers(rs);
			if(users.size() != 0) {
				return users.get(0);
			}
			return null;
		} catch(SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public void update(Connection connection, User user) {
		PreparedStatement ps = null;

		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET ");
			sql.append("account = ?, ");
			if(!StringUtils.isBlank(user.getPassword())) {
				sql.append("password = ?, ");
			}
			sql.append("name = ?, ");
			sql.append("branch_id = ?, ");
			sql.append("department_id = ?, ");
			sql.append("updated_date = CURRENT_TIMESTAMP ");
			sql.append("WHERE id = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, user.getAccount());
			if(!StringUtils.isBlank(user.getPassword())) {
				ps.setString(2, user.getPassword());
				ps.setString(3, user.getName());
				ps.setInt(4, user.getBranchId());
				ps.setInt(5, user.getDepartmentId());
				ps.setInt(6, user.getId());
			} else {
				ps.setString(2, user.getName());
				ps.setInt(3, user.getBranchId());
				ps.setInt(4, user.getDepartmentId());
				ps.setInt(5, user.getId());
			}

			int updateCount = ps.executeUpdate();
			if(updateCount == 1) {
				System.out.println("更新成功");
			} else {
				System.out.println("更新失敗");
				throw new NoRowsUpdatedRuntimeException();
			}
		} catch(SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public void update(Connection connection, int userId, int isStopped) {
		PreparedStatement ps = null;

		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET ");
			sql.append("is_stopped = ? ");
			sql.append("WHERE id = ?");

			ps = connection.prepareStatement(sql.toString());
			ps.setInt(1, isStopped);
			ps.setInt(2, userId);

			int updateCount = ps.executeUpdate();
			if(updateCount == 1) {
				System.out.println("更新成功");
			} else {
				System.out.println("更新失敗");
				throw new NoRowsUpdatedRuntimeException();
			}
		} catch(SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<User> toUsers(ResultSet rs) throws SQLException {
		List<User> users = new ArrayList<>();
		try {
			while(rs.next()) {
				User user = new User();
				user.setId(rs.getInt("id"));
				user.setAccount(rs.getString("account"));
				user.setPassword(rs.getString("password"));
				user.setName(rs.getString("name"));
				user.setBranchId(rs.getInt("branch_id"));
				user.setDepartmentId(rs.getInt("department_id"));
				user.setIsStopped(rs.getInt("is_stopped"));
				user.setCreatedDate(rs.getDate("created_date"));
				user.setUpdatedDate(rs.getDate("updated_date"));
				users.add(user);
			}
			return users;
		} finally {
			close(rs);
		}

	}
}
