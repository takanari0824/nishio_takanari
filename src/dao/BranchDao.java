package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.Branch;
import exception.SQLRuntimeException;

public class BranchDao {

	public List<Branch> select(Connection connection){
		PreparedStatement ps = null;

		try {
			String sql = "SELECT * FROM branches";
			ps = connection.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			List<Branch> branchList = toBranchList(rs);
			return branchList;
		} catch(SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<Branch> toBranchList(ResultSet rs) throws SQLException{
		List<Branch> branchList = new ArrayList<>();

		try {
			while(rs.next()) {
				Branch branch = new Branch();
				branch.setId(rs.getInt("id"));
				branch.setName(rs.getString("name"));
				branch.setCreatedDate(rs.getDate("created_date"));
				branch.setUpdatedDate(rs.getDate("updated_date"));
				branchList.add(branch);
			}
			return branchList;
		} finally {
			close(rs);
		}

	}

}
