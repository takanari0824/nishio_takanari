package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.Department;
import exception.SQLRuntimeException;

public class DepartmentDao {

	public List<Department> select(Connection connection){
		PreparedStatement ps = null;

		try {
			String sql = "SELECT * from departments";

			ps = connection.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			List<Department> departmentList = toDepartmentList(rs);
			return departmentList;
		} catch(SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<Department> toDepartmentList(ResultSet rs) throws SQLException{
		List<Department> departmentList = new ArrayList<>();

		try {
			while(rs.next()) {
				Department department = new Department();
				department.setId(rs.getInt("id"));
				department.setName(rs.getString("name"));
				department.setCreatedDate(rs.getDate("created_date"));
				department.setUpdatedDate(rs.getDate("updated_date"));
				departmentList.add(department);
			}
			return departmentList;
		} finally {
			close(rs);
		}

	}
}
