package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.UserMessage;
import exception.SQLRuntimeException;

public class UserMessageDao {

	public List<UserMessage> select(Connection connection, String start, String end, String category){
		PreparedStatement ps = null;

		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("messages.id AS id, ");
			sql.append("messages.title AS title, ");
			sql.append("messages.text AS text, ");
			sql.append("messages.category AS category, ");
			sql.append("users.name AS userName, ");
			sql.append("users.id AS userId, ");
			sql.append("messages.created_date AS createdDate, ");
			sql.append("messages.updated_date AS updatedDate ");
			sql.append("FROM messages ");
			sql.append("INNER JOIN users ");
			sql.append("ON messages.user_id = users.id ");
			sql.append("WHERE messages.created_date BETWEEN ? AND ? ");
			if(category != null) {
				sql.append("AND messages.category LIKE ? ");
			}
			sql.append("ORDER BY createdDate DESC");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, start);
			ps.setString(2, end);
			if(category != null) {
				ps.setString(3, category);
			}

			ResultSet rs = ps.executeQuery();

			List<UserMessage> userMessages = toUserMessages(rs);
			return userMessages;
		} catch(SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<UserMessage> toUserMessages(ResultSet rs) throws SQLException {
		List<UserMessage> userMessages = new ArrayList<>();
		try {
			while(rs.next()) {
				UserMessage userMessage = new UserMessage();
				userMessage.setId(rs.getInt("id"));
				userMessage.setTitle(rs.getString("title"));
				userMessage.setText(rs.getString("text"));
				userMessage.setCategory(rs.getString("category"));
				userMessage.setUserName(rs.getString("userName"));
				userMessage.setUserId(rs.getInt("userId"));
				userMessage.setCreatedDate(rs.getTimestamp("createdDate"));
				userMessage.setUpdatedDate(rs.getTimestamp("updatedDate"));
				userMessages.add(userMessage);
			}
			return userMessages;
		} finally {
			close(rs);
		}

	}
}
