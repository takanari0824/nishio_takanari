package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.UserBranchDepartment;
import exception.SQLRuntimeException;

public class UserBranchDepartmentDao {

	public List<UserBranchDepartment> select(Connection connection){
		PreparedStatement ps = null;

		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("users.id AS id, ");
			sql.append("users.account AS account, ");
			sql.append("users.name AS name, ");
			sql.append("users.is_stopped AS isStopped, ");
			sql.append("branches.id AS branchId, ");
			sql.append("branches.name AS branchName, ");
			sql.append("departments.id AS departmentId, ");
			sql.append("departments.name AS departmentName ");
			sql.append("FROM users ");
			sql.append("INNER JOIN branches ");
			sql.append("ON users.branch_id = branches.id ");
			sql.append("INNER JOIN departments ");
			sql.append("ON users.department_id = departments.id");

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();

			List<UserBranchDepartment> userBranchDepartments = toUserBranchDepartments(rs);
			return userBranchDepartments;
		} catch(SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}

	}

	private List<UserBranchDepartment> toUserBranchDepartments(ResultSet rs) throws SQLException{
		List<UserBranchDepartment> userBranchDepartments = new ArrayList<>();

		try {
			while(rs.next()) {
				UserBranchDepartment user = new UserBranchDepartment();
				user.setId(rs.getInt("id"));
				user.setAccount(rs.getString("account"));
				user.setName(rs.getString("name"));
				user.setIsStopped(rs.getInt("isStopped"));
				user.setBranchId(rs.getInt("branchId"));
				user.setBranchName(rs.getString("branchName"));
				user.setDepartmentId(rs.getInt("departmentId"));
				user.setDepartmentName(rs.getString("departmentName"));
				userBranchDepartments.add(user);
			}
			return userBranchDepartments;
		} finally {
			close(rs);
		}

	}

}
