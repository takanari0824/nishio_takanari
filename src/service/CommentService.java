package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.Comment;
import beans.UserComment;
import dao.CommentDao;
import dao.UserCommentDao;

public class CommentService {

	public void insert(Comment comment) {
		Connection connection = null;

		try {
			connection = getConnection();
			new CommentDao().insert(connection, comment);
			commit(connection);

		} catch(RuntimeException e) {
			rollback(connection);
			throw e;
		} catch(Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public List<UserComment> select(){
		Connection connection = null;

		try {
			connection = getConnection();
			List<UserComment> userComments = new UserCommentDao().select(connection);
			commit(connection);
			return userComments;

		} catch(RuntimeException e) {
			rollback(connection);
			throw e;
		} catch(Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public void delete(int id) {
		Connection connection = null;

		try {
			connection = getConnection();
			new CommentDao().delete(connection, id);
			commit(connection);
		} catch(RuntimeException e) {
			rollback(connection);
			throw e;
		} catch(Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

}
