package filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;

@WebFilter("/*")
public class LoginFilter implements Filter {

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;
		HttpSession session = req.getSession();
		User user = (User) session.getAttribute("loginUser");

		String servletPath = req.getServletPath();

		if(servletPath.equals("/login")) {
			chain.doFilter(request, response);
		} else if(user != null) {
			chain.doFilter(request, response);
		} else {
			List<String> errorMessages = new ArrayList<>();
			errorMessages.add("ログインしてください。");
			session.setAttribute("errorMessages", errorMessages);
			res.sendRedirect("./login");
		}



	}

	public void init(FilterConfig config) {

	}

	public void destroy() {

	}
}
