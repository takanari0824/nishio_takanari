package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Comment;
import beans.User;
import service.CommentService;

@WebServlet(urlPatterns = {"/comment"})
public class CommentServlet extends HttpServlet {

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{

		List<String> errorMessages = new ArrayList<>();
		Comment comment = getComment(request);

		if(!isValid(comment, errorMessages)) {
			HttpSession session = request.getSession();
			session.setAttribute("errorMessages", errorMessages);
			response.sendRedirect("./");
			return;
		}

		new CommentService().insert(comment);
		response.sendRedirect("./");

	}

	private Comment getComment(HttpServletRequest request) {
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("loginUser");
		Comment comment = new Comment();
		comment.setText(request.getParameter("text"));
		comment.setUserId(user.getId());
		comment.setMessageId(Integer.parseInt(request.getParameter("messageId")));
		return comment;
	}

	private boolean isValid(Comment comment, List<String> errorMessages) {
		String text = comment.getText();

		if(StringUtils.isBlank(text)) {
			errorMessages.add("本文を入力してください。");
		}

		if(!StringUtils.isBlank(text) && text.length() > 500) {
			errorMessages.add("本文は500文字以下で入力してください。");
		}

		if(errorMessages.size() == 0) {
			return true;
		}
		return false;
	}
}
