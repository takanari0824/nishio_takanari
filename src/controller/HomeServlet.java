package controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import beans.UserComment;
import beans.UserMessage;
import service.CommentService;
import service.MessageService;

@WebServlet(urlPatterns = {"/index.jsp"})
public class HomeServlet extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String end = sdf.format(new Date());
		String start = "2020-01-01 00:00:00";
		String category = null;

		String selectedStart = request.getParameter("start");
		if(!StringUtils.isBlank(selectedStart)) {
			start = selectedStart + " 00:00:00";
		}

		String selectedEnd = request.getParameter("end");
		if(!StringUtils.isBlank(selectedEnd)) {
			end = selectedEnd + " 23:59:59";
		}

		String selectedCategory = request.getParameter("category");
		if(!StringUtils.isBlank(selectedCategory)) {
			category = "%" + selectedCategory + "%";
		}


		List<UserMessage> userMessages = new MessageService().select(start, end, category);
		request.setAttribute("userMessages", userMessages);

//		絞り込みフォームにstart/dateを送る
		request.setAttribute("selectedStart", selectedStart);
		request.setAttribute("selectedEnd", selectedEnd);
		request.setAttribute("selectedCategory", selectedCategory);

		List<UserComment> userComments = new CommentService().select();
		request.setAttribute("userComments", userComments);

		request.getRequestDispatcher("home.jsp").forward(request, response);
		}
}
