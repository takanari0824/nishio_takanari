package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Message;
import beans.User;
import service.MessageService;

@WebServlet(urlPatterns = {"/message"})
public class MessageServlet extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		request.getRequestDispatcher("message.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		List<String> errorMessages = new ArrayList<>();
		Message message = getMessage(request);

		if(!isValid(message, errorMessages)) {
			request.setAttribute("message", message);
			request.setAttribute("errorMessages", errorMessages);
			request.getRequestDispatcher("message.jsp").forward(request, response);
			return;
		}

		new MessageService().insert(message);
		response.sendRedirect("./");
	}

	private Message getMessage(HttpServletRequest request) {
		HttpSession session = request.getSession();
		User user = (User)session.getAttribute("loginUser");
		Message message = new Message();
		message.setTitle(request.getParameter("title"));
		message.setText(request.getParameter("text"));
		message.setCategory(request.getParameter("category"));
		message.setUserId(user.getId());
		return message;
	}

	private boolean isValid(Message message, List<String> errorMessages) {
		String title = message.getTitle();
		String text = message.getText();
		String category = message.getCategory();

		if(StringUtils.isBlank(title)) {
			errorMessages.add("件名を入力してください。");
		}

		if(!StringUtils.isBlank(title) && title.length() > 30) {
			errorMessages.add("件名は30文字以下で入力してください。");
		}

		if(StringUtils.isBlank(text)) {
			errorMessages.add("投稿内容を入力してください。");
		}

		if(!StringUtils.isBlank(text) && text.length() > 1000) {
			errorMessages.add("投稿内容は1000文字以下で入力してください。");
		}

		if(StringUtils.isBlank(category)) {
			errorMessages.add("カテゴリーを入力してください。");
		}

		if(!StringUtils.isBlank(category) && category.length() > 10) {
			errorMessages.add("カテゴリーは10文字以下で入力してください。");
		}


		if(errorMessages.size() == 0) {
			return true;
		}
		return false;
	}
}
