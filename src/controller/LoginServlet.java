package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.User;
import service.UserService;

@WebServlet(urlPatterns = {"/login"})
public class LoginServlet extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		request.getRequestDispatcher("login.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		String account = request.getParameter("account");
		String password = request.getParameter("password");

		List<String> errorMessages = new ArrayList<>();

		if(!isValid(account, password, errorMessages)){
			request.setAttribute("errorMessages", errorMessages);
			request.setAttribute("account", account);
			request.getRequestDispatcher("login.jsp").forward(request, response);
			return;
		}

		User user = new UserService().select(account, password);

		if(user == null || user.getIsStopped() == 1) {
//			DBにユーザー情報なかった場合、もしくはisStoppedが1の場合
			errorMessages.add("ログインに失敗しました");
			request.setAttribute("errorMessages", errorMessages);
			request.setAttribute("account", account);
			request.getRequestDispatcher("login.jsp").forward(request, response);
			return;
		}

		HttpSession session = request.getSession();
		session.setAttribute("loginUser", user);
		response.sendRedirect("./");

	}

	private boolean isValid(String account, String password, List<String> errorMessages) {
		if(StringUtils.isBlank(account)) {
			errorMessages.add("アカウントが入力されていません。");
		}

		if(StringUtils.isBlank(password)) {
			errorMessages.add("パスワードが入力されていません。");
		}

		if(errorMessages.size() == 0) {
			return true;
		}
		return false;
	}
}
