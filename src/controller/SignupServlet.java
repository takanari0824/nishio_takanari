package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import beans.Branch;
import beans.Department;
import beans.User;
import service.BranchService;
import service.DepartmentService;
import service.UserService;

@WebServlet(urlPatterns = {"/signup"})
public class SignupServlet extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		List<Branch> branchList = new BranchService().select();
		List<Department> departmentList = new DepartmentService().select();

		request.setAttribute("branchList", branchList);
		request.setAttribute("departmentList", departmentList);

		request.getRequestDispatcher("signup.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		List<String> errorMessages = new ArrayList<>();

		User user = getUser(request);

		String passwordConfirmation = request.getParameter("passwordConfirmation");
		if(!isValid(user, passwordConfirmation, errorMessages)) {
			List<Branch> branchList = new BranchService().select();
			List<Department> departmentList = new DepartmentService().select();
			request.setAttribute("branchList", branchList);
			request.setAttribute("departmentList", departmentList);
			request.setAttribute("user", user);
			request.setAttribute("errorMessages", errorMessages);
			request.getRequestDispatcher("signup.jsp").forward(request, response);
			return;
		}

		new UserService().insert(user);
		response.sendRedirect("./management");
	}

	private User getUser(HttpServletRequest request) {
		User user = new User();
		user.setAccount(request.getParameter("account"));
		user.setPassword(request.getParameter("password"));
		user.setName(request.getParameter("name"));
		user.setBranchId(Integer.parseInt(request.getParameter("branchId")));
		user.setDepartmentId(Integer.parseInt(request.getParameter("departmentId")));
		return user;
	}

	private boolean isValid(User user,String passwordConfirmation, List<String> errorMessages) {
		String account = user.getAccount();
		String password = user.getPassword();
		String name = user.getName();
		int branchId = user.getBranchId();
		int departmentId = user.getDepartmentId();

		if(StringUtils.isBlank(account)) {
			errorMessages.add("アカウント名を入力してください。");
		}

		if(!StringUtils.isBlank(account) && !account.matches("^[a-zA-Z0-9]{6,20}$")) {
			errorMessages.add("アカウント名は6文字以上20文字以下の半角英数字で入力してください。");
		}

		if(StringUtils.isBlank(password)) {
			errorMessages.add("パスワードを入力してください");
		}

		if(!StringUtils.isBlank(password) && !password.equals(passwordConfirmation)) {
			errorMessages.add("パスワードと確認用パスワードが一致していません。");
		}

		if(!StringUtils.isBlank(password) && password.equals(passwordConfirmation) && !password.matches("^[ -~]{6,20}$")) {
			errorMessages.add("パスワードは6文字以上20文字以内の半角文字で入力して下さい。");
		}

		if(StringUtils.isBlank(name)) {
			errorMessages.add("名前を入力してください。");
		}

		if(!StringUtils.isBlank(name) && !name.matches("^.{1,10}")) {
			errorMessages.add("名前は10文字以内で入力してください。");
		}

		if((branchId == 1 && (departmentId == 3 || departmentId == 4)) || (branchId != 1 && (departmentId == 1 || departmentId == 2)) ) {
			errorMessages.add("支社と部署の組み合わせが不正です。");
		}

		if(isExisted(account)) {
			errorMessages.add("重複したアカウントは登録できません。");
		}

		if(errorMessages.size() == 0) {
			return true;
		}
		return false;
	}

	private boolean isExisted(String account) {
		User user = new UserService().select(account);
		if(user == null) {
			return false;
		}
		return true;
	}
}
