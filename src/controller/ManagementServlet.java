package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.UserBranchDepartment;
import service.UserService;

@WebServlet(urlPatterns = {"/management"})
public class ManagementServlet extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		List<UserBranchDepartment> userBranchDepartments = new UserService().select();
		request.setAttribute("userBranchDepartments", userBranchDepartments);
		request.getRequestDispatcher("management.jsp").forward(request, response);
	}
}
